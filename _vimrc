call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

set hidden
set number

command! Status echo "yo"
colorscheme morning

if has("autocmd")
	filetype plugin indent on
endif
